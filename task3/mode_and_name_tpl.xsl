<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--вывод элементов для head-->
    <xsl:template name="head_brusher">
        <xsl:param name="header_node" />
        <xsl:copy-of select="$header_node" />
    </xsl:template>

        <xsl:template match="head/child::node()" mode="header_elements">
            <xsl:copy-of select="."/>
        </xsl:template>


<!--вывод атрибутов в форме-->
    <xsl:template match="submission/@*" mode="form_id_brusher">
        <xsl:attribute name="{name()}">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

<!--вывод атрибута в форме для события  onsubmit-->
    <xsl:template match="bind/@required" mode="form_event_onsubmit">
        <xsl:if test="not(contains(@required,'..'))">
        <!--поиск элемента bind у которого в атрибуте required прописана зависимость
        на значение друго элемента,т.е - если в выходном HTML элемент с id=peniOn имеет value=true
        то валидация для элемента id=peni нужна,такой зависимый элемент по моей выдуманной задаче может быть один-->
            <xsl:variable name="obj">
                <xsl:apply-templates select="$fields_rules[contains(@required,'..')]" mode="min_values_object"/>
            </xsl:variable>
            <!--чистит пробельные символы-->
            <xsl:variable name="obj_clear">
                <xsl:value-of select="translate(normalize-space($obj),' ','')"/>
            </xsl:variable>
            <!--итого в аргумент для checkValidator летит объект вида -
            {на какой id зависимость:bool,какой обект зависим:правило валидации}-->
            <xsl:attribute name="onsubmit">checkValidator({<xsl:value-of select="substring-before(substring-after(.,'/'),'=')"/>:<xsl:value-of select="substring-after(.,'=')"/>,<xsl:value-of select="substring($obj_clear,1,string-length($obj_clear)-1)"/>});return false;</xsl:attribute>
        </xsl:if>
    </xsl:template>



<!--выводит из элемента bind строку в виде @nodeset:@constraint-->
    <xsl:template match="/html/head/model/bind" mode="min_values_object">
        <xsl:value-of select="@nodeset"/>:'<xsl:value-of select="substring-after(@constraint,' ')"/>',
    </xsl:template>

<!--ключ составлен по дочерним элементам body,в принимающий агрумент ключа идут дочернии name() элемента data,
 т.е идет связывание дочерних name() элемента data и дочерних @ref элемента body-->
    <xsl:template match="data/child::node()" mode="switchOnKey">
        <!--см.form_kids_creator.xsl-->
        <xsl:apply-templates select="key('ref_index',name())"/>
    </xsl:template>


<!--добавляет атрибут для скрытия элемента-->
    <xsl:template name="elementIsNotVisible">
        <xsl:attribute name="style">
            <xsl:value-of select="'display:none;'"/>
        </xsl:attribute>
    </xsl:template>


<!--добавляет атрибут value для значений по умолчанию-->
    <xsl:template match="/html/head/model/instance/data/child::node()" mode="value_attr">
        <xsl:attribute name="value">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>


<!--создает и заполняет элемент option,выставляет значение по умолчанию-->
    <xsl:template match="item/child::node()" mode="elementIsSelectOption">
        <!--приходит из вызывающего-->
        <xsl:param name="default_month"/>
        <xsl:param name="value" select="self::node()/following-sibling::*[1]/self::value"/>
        <xsl:if test="name()='label'">
            <option>
                <xsl:attribute name="value">
                    <xsl:value-of select="$value"/>
                </xsl:attribute>
                <!--выставляет option по умолчанию-->
                <xsl:if test="key('default_value_index',$default_month)=$value">
                    <xsl:attribute name="selected"/>
                </xsl:if>
                <xsl:value-of select="."/>
            </option>
        </xsl:if>
    </xsl:template>


<!--для инпута с типом чекбокс,задает атрибут onchange-->
    <xsl:template match="/html/head/model/bind" mode="event_on_change">
        <xsl:param name="referenceIdName"/><!--id инпута с checkbox-->
        <xsl:if test="contains(@required,$referenceIdName)">
            <xsl:attribute name="onchange">valueChanged({<xsl:value-of select="$referenceIdName"/>:'<xsl:value-of select="@nodeset"></xsl:value-of>'})</xsl:attribute>
        </xsl:if>
    </xsl:template>

<!--блок под ошибку-->
    <xsl:template match="/html/head/model/bind" mode="draw_error_field">
        <span class="error" style="display:none;color:red;">
            <xsl:value-of select="key('default_value_index','error')"/>
        </span>
    </xsl:template>

 <!--определяет каким элементам нужна валидация   -->
    <xsl:template name="validate_inspect">
        <xsl:param name="inspect_name"/><!--@ref из дочерних элемента body-->
        <xsl:param name="element_required_attr" select="key('nodeset_index',$inspect_name)"/>
        <!--под элементы с атрибутом required создается css класс-флаг необходимости заполнения поля-->
        <xsl:choose>
            <xsl:when test="$element_required_attr/@required='true()'">
                <xsl:attribute name="class">
                    <xsl:value-of select="name($element_required_attr/@required)"/>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>
        <!--атрибут maxlength-->
        <xsl:if test="string-length($element_required_attr/@maxlength) &gt; 0">
            <xsl:attribute name="{name($element_required_attr/@maxlength)}">
                <xsl:value-of select="$element_required_attr/@maxlength"/>
            </xsl:attribute>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>