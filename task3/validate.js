/**
 * Вызывается при сабмите формы.
 * 1)через relationCheck(obj) проверяет поставлен ли чекбокс,
 * если чекбокс поставлен,то применяет правило валидации
 * 2)элементы с классом required проверяет на пустоту в значении через checkEmptyElementsClassRequired()
 * 3)проверяет есть ли в документе видимые блоки сообщений об ошибках через  isErrorElementsExist()
 *
 * @param Object obj {id элемента с чекбоксом:значение элемента с чекбоксом,
 *                    id элемента подписанного на чекбокс:правило валидации элемента}
 */
function checkValidator(obj){

    relationCheck(obj);
    checkIsEmptyElementClassRequired();
    isErrorElementsExist();

}


/**
 * Применяет правило валидации для элемента подписанного на чекбокс
 * @param Object obj {id элемента с чекбоксом:значение элемента с чекбоксом,
 *                    id элемента подписанного на чекбокс:правило валидации элемента}
 */
function relationCheck(obj){
    var idInputCheckBox=Object.keys( obj )[0]+'';//id элемента с чекбоксом
    var idInputShow=Object.keys( obj )[1]+'';//id элемента подписанного на чекбокс
    if($('#'+idInputCheckBox).val()=='true'){
        applyValidateRule(obj[idInputShow],idInputShow);
    }
}


/**
 * Проверка элементов с классом required на пустоту в значении,
 * если значение пустое- показывает ошибку
 */
function checkIsEmptyElementClassRequired(){
    $('.required').each(function () {
        var errorContentBlock=$(this).next();
        if(!$(this).val().length){
            errorContentBlock.show();
        }else{
            errorContentBlock.hide();
        }

    });
}


/**
 * Если  нет сообщений с ошибками,то успех
 */
function isErrorElementsExist(){
    if(!($('.error').is(':visible'))) {
        $('form').hide();
        $('#success').show();
    }
}


/**
 * Применяет правило валидации,
 * показывает ошибки если валидация не пройдена
 *
 * @param rule
 * @param id
 */
function applyValidateRule(rule,id){
    var idLocal=$('#'+id);
    if(rule.indexOf(">") >= 0){
        if(!(idLocal.val()>0)){
            idLocal.next().show();
        }else{
            idLocal.next().hide();
        }
    }
}


/**
 * Показывает/скрывает подписанный на чекбокс блок
 *
 * @param Object obj  ключ-id элемента на котором чекбокс,значение-id элемента который show/hide по клику на чекбокс
 */
function valueChanged(obj){
    var idInputCheckBox=Object.keys( obj )[0]+'';// привидение к строке
    var idInputShowHide=obj[idInputCheckBox];

    if($('#'+idInputCheckBox).is(":checked")){
        $('#'+idInputCheckBox).attr('value','true');
        $('#'+idInputShowHide).show();
        $('label[for="'+idInputShowHide+'"]').show();
    }
    else {
        $('#'+idInputCheckBox).attr('value','false');
        $('#'+idInputShowHide).hide();
        $('label[for="'+idInputShowHide+'"]').hide();
        /*скрывает блок с ошибкой,при снятии галки с чекбокса,т.к
        валидация на зависимый от чекбокса элемент не нужна*/
        $('#'+idInputShowHide).next().hide();
    }
}


/**
 * Вызывается на событие onclick  элемента input type='submit',
 * Задает минимальное кол-во символов в значении элемента
 *
 * @param Object $obj {'id элемента','правило валидации'}
 */
function  setMinLength($obj){
    for(var id in $obj){
        applyValidateRule($obj[id],id);
    }


}