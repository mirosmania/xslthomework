<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--заполнение формы child элементами-->
    <xsl:template match="/html/body/child::node()">
        <p>
            <!--label-->
            <xsl:element name="{name(self::node()/child::*[1])}">
                <xsl:attribute name="for">
                    <xsl:value-of select="@ref"/>
                </xsl:attribute>
                <xsl:if test="@ref='peni'">
                    <xsl:call-template name="elementIsNotVisible"/>
                </xsl:if>
                <xsl:value-of select="self::node()/child::*[1]"/>
            </xsl:element>
            <!--field-->
            <xsl:choose>
                <!--если не чекбокс-->
                <xsl:when test="not(@ref='peniOn')">
                    <xsl:element name="{name()}">
                        <xsl:attribute name="name">
                            <xsl:value-of select="@ref"/>
                        </xsl:attribute>
                        <xsl:attribute name="id">
                            <xsl:value-of select="@ref"/>
                        </xsl:attribute>
                        <xsl:attribute name="type">
                            <xsl:value-of select="'text'"/>
                        </xsl:attribute>
                        <!--правила валидации-->
                        <xsl:call-template name="validate_inspect">
                            <xsl:with-param name="inspect_name" select="@ref"/>
                        </xsl:call-template>
                        <xsl:if test="@ref='peni'">
                            <!--делает элемент скрытым display:none-->
                            <xsl:call-template name="elementIsNotVisible"/>
                        </xsl:if>

                        <xsl:choose>
                            <!--select c option-->
                            <xsl:when test="name()='select' and count(self::node()/item)>1">
                                <xsl:apply-templates select="self::node()/item/child::node()" mode="elementIsSelectOption">
                                    <xsl:with-param name="default_month" select="@ref"/>
                                </xsl:apply-templates>
                            </xsl:when>
                            <!--для остальных, если есть, выводит value по умолчанию-->
                            <xsl:otherwise>
                                <xsl:apply-templates select="key('default_value_index',@ref)" mode="value_attr"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="input">
                        <xsl:attribute name="name">
                            <xsl:value-of select="@ref"/>
                        </xsl:attribute>
                        <xsl:attribute name="id">
                            <xsl:value-of select="@ref"/>
                        </xsl:attribute>
                        <xsl:attribute name="type">
                            <xsl:value-of select="'checkbox'"/>
                        </xsl:attribute>
                        <!--событие onchange-->
                        <xsl:apply-templates select="/html/head/model/bind" mode="event_on_change">
                            <xsl:with-param name="referenceIdName">
                                <xsl:value-of select="@ref"/>
                            </xsl:with-param>
                        </xsl:apply-templates>
                        <!--value по умолчанию-->
                        <xsl:apply-templates select="key('default_value_index',@ref)" mode="value_attr"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
            <!--блок под ошибку-->
            <xsl:apply-templates select="key('nodeset_index',@ref)" mode="draw_error_field"/>
        </p>
        <!--сработает на предполследний child элемента body-->
        <xsl:if test="$element_before_submit=@ref">
            <xsl:variable name="obj">
                <!--сработает на элементы bind у которых есть атрибут constraint и атрибут required
                не ссылающийся на другой элемент,в переменную придет строка @nodeset:@constraint-->
                <xsl:apply-templates select="$fields_rules[string-length(@constraint) &gt; 0 and not(contains(@required,'..'))]" mode="min_values_object"/>
            </xsl:variable>
            <!--чистит пробельные символы-->
            <xsl:variable name="obj_clear">
                <xsl:value-of select="translate(normalize-space($obj),' ','')"/>
            </xsl:variable>
            <p>
                <!--выводит submit-->
                <xsl:element name="input">
                    <xsl:attribute name="type">
                        <xsl:value-of select="name(following-sibling::*[1])"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="following-sibling::*[1]/label"/>
                    </xsl:attribute>
                    <!--мин длина-->
                    <!--выводит,без последнего символа(запятая пришедшая в $obj)-->
                    <xsl:attribute name="onclick">setMinLength({<xsl:value-of select="substring($obj_clear,1,string-length($obj_clear)-1)"/>})</xsl:attribute>
                </xsl:element>
            </p>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>