<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--для заполнения формы child элементами по дочерним элементам body c атрибутом ref-->
    <xsl:key name="ref_index" match="/html/body/child::node()" use="@ref"/>

    <!--для  валидапции-->
    <xsl:key name="nodeset_index" match="/html/head/model/bind" use="@nodeset"/>

    <!--для заполнения атрибута value значением по умолчанию-->
    <xsl:key name="default_value_index" match="/html/head/model/instance/data/child::node()[normalize-space()]" use="name()"/>

    <xsl:variable name="fields" select="/html/head/model/instance/data"/>

    <!--предпоследний child элемента body-->
    <xsl:variable name="element_before_submit" select="/html/body/*[last()-1]/@ref"/>

    <xsl:variable name="fields_rules" select="$fields/parent::node()/following-sibling::node()/self::bind"/>

</xsl:stylesheet>