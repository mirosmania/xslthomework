<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


    <!--содержит ключи и переменные-->
    <xsl:import href="keys_and_variables.xsl"/>

    <!--содержит именованные и mode шаблоны-->
    <xsl:import href="mode_and_name_tpl.xsl"/>

    <!--шаблон заполнения формы дочерними элементами-->
    <xsl:import href="form_kids_creator.xsl"/>

    <!--html-->
    <xsl:template match="/html">
        <xsl:element name="{name()}">
            <xsl:apply-templates select="head/model"/>
        </xsl:element>
    </xsl:template>


    <xsl:template match="model">
        <!--head-->
        <xsl:element name="{name(parent::node())}">
            <xsl:call-template name="head_brusher">
                <xsl:with-param name="header_node">
                    <!--верхние соседи model-->
                    <xsl:apply-templates select="preceding-sibling::node()" mode="header_elements"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:element>

        <!--body-->
        <xsl:element name="{name(parent::node()/following-sibling::*[1]/self::body)}">
            <xsl:element name="form">
                <!--атрибуты для формы-->
                <xsl:apply-templates select="submission/@*" mode="form_id_brusher"/>
                <xsl:apply-templates select="bind/@required" mode="form_event_onsubmit"/>
                <!--заполнение формы child элементами по ключю ref_index,
                    в принимающий аргумент ключа идут дочернии name() элемента data-->
                <xsl:apply-templates select="$fields/child::node()" mode="switchOnKey"/>
            </xsl:element>
            <!--блок успеха,по умолчанию-скрыт-->
            <xsl:element name="div">
                <xsl:attribute name="id">
                    <xsl:value-of select="'success'"/>
                </xsl:attribute>
                <xsl:call-template name="elementIsNotVisible"/>
                <xsl:element name="h1">
                    <xsl:value-of select="key('default_value_index','success')"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>