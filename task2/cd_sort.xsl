<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:exsl="http://exslt.org/common"
        exclude-result-prefixes="exsl">

<!--параметры xsltproc-->
    <xsl:param name="order"/>
    <xsl:param name="sort">artist</xsl:param>
    <xsl:param name="year"/>
    <xsl:param name="artist"/>

    <!--локальная переменная сортировки завязанная под параметр sort-->
    <xsl:variable name="sort_way">
        <by type="text" name_for_user="artist">artist</by>
        <by type="text" name_for_user="album">album</by>
        <by type="number" name_for_user="year">year_release</by>
        <by type="text" name_for_user="studio">record_studio</by>
    </xsl:variable>

    <!--локальная переменная направления сортировки завязанная под параметр order-->
    <xsl:variable name="order_way">
        <by>ascending</by>
        <by choice="not_default">descending</by>
    </xsl:variable>

    <!--ключ для группировки по году-->
    <xsl:key name="year_index" match="music_collection/audio_cd" use="@year_release"/>





<!--главный шаблон-->
    <xsl:template match="/music_collection">
        <html>
            <body>
            <table border="1">
                <th>год</th>
                <th>артист</th>
                <th>альбом</th>
                <th>студия</th>
                <th>список треков</th>
                <th>ссылка на обложку</th>
                <xsl:choose>
                    <!--если передан параметр для обратного направления сортировки-->
                    <xsl:when test="$order='desc'">
                        <xsl:call-template name="sorter">
                            <xsl:with-param name="order_param">
                                <xsl:apply-templates select="exsl:node-set($order_way)/by[@choice='not_default']"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:when>
                    <!--направление сортировки по умолчанию-->
                    <xsl:otherwise>
                        <xsl:call-template name="sorter">
                            <xsl:with-param name="order_param">
                                <xsl:apply-templates select="exsl:node-set($order_way)/by[1]"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </table>
            </body>
        </html>
    </xsl:template>


    <!--Если передан параметр $year,то переход на шаблон year_param_is_exist
     где группируется по году релиза альбома,затем переход на шаблон only_that_year.Если параметр $year
     не передан, то сразу переход на шаблон only_that_year-->
    <xsl:template name="sorter">
        <!--параметр сортировки который приходит из главного шаблона-->
        <xsl:param name="order_param" />
        <xsl:choose>
            <xsl:when test="normalize-space($year)">
                <xsl:call-template name="year_param_is_exist">
                    <xsl:with-param name="order_param" select="$order_param"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="only_that_year">
                    <xsl:with-param name="set_after_group_by_year">
                        <xsl:apply-templates select="audio_cd">
                            <xsl:sort data-type="{exsl:node-set($sort_way)/by[@name_for_user=$sort]/@type}" lang="en" order="{$order_param}" select="@*[name()=exsl:node-set($sort_way)/by[@name_for_user=$sort]]" />
                        </xsl:apply-templates>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!--передан параметр $year, группировка по году выпуска альбома(в параметр set_after_group_by_year в виде временного дерева),
    затем переход на шаблон only_that_year-->
    <xsl:template name="year_param_is_exist">
        <xsl:param name="order_param" />
        <xsl:call-template name="only_that_year">
            <xsl:with-param name="set_after_group_by_year">
                <xsl:apply-templates select="key('year_index',$year)" >
                    <xsl:sort data-type="{exsl:node-set($sort_way)/by[@name_for_user=$sort]/@type}" lang="en" order="{$order_param}" select="@*[name()=exsl:node-set($sort_way)/by[@name_for_user=$sort]]" />
                </xsl:apply-templates>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>


    <!-- выборка из временного дерева,параметра set_after_group_by_year-->
    <xsl:template name="only_that_year">
        <xsl:param name="set_after_group_by_year" />
        <xsl:choose>
            <xsl:when test="normalize-space($artist)">
                <xsl:apply-templates select="exsl:node-set($set_after_group_by_year)/audio_cd[@artist=$artist]" mode="draw"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="exsl:node-set($set_after_group_by_year)/audio_cd" mode="draw"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="audio_cd" >
        <xsl:copy-of  select="." />
    </xsl:template>




    <xsl:template match="audio_cd" mode="draw">
        <tr>
            <td>
                <xsl:value-of select="@year_release"/>
            </td>
            <td>
                <xsl:value-of select="@artist"/>
            </td>
            <td>
                <xsl:value-of select="@album"/>
            </td>
            <td>
                <xsl:value-of select="@record_studio"/>
            </td>

            <td>
                <ul>
                     <xsl:apply-templates select="track_list/track"/>
                </ul>
            </td>
            <td>
                 <img src="{@album_cover}"/>
            </td>
        </tr>
    </xsl:template>


    <xsl:template match="track">
        <li>
            <xsl:value-of select="."/>
        </li>
    </xsl:template>


</xsl:stylesheet>


