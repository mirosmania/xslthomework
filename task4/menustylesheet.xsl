<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:exsl="http://exslt.org/common"
        exclude-result-prefixes="exsl">
    <xsl:output method="html"
                doctype-public="-//W3C//DTD HTML 4.01//EN"
                doctype-system="http://www.w3.org/TR/html4/strict.dtd"/>


<!--nodeset для задания (a)-->
    <xsl:param name="menu">
            <item>
                <title>Новости</title>
                <url is_current="false()">/news.html</url>
            </item>
            <item>
                <title>Погода</title>
                <url is_current="false()">/weather.html</url>
            </item>
            <item>
                <title>Спорт</title>
                <url is_current="true()">/sport.html</url>
            </item>
            <item>
                <title>Финансы</title>
                <url is_current="false()">/finance.html</url>
            </item>
    </xsl:param>

<!--параметр для задания (б),по умолчанию пустое значение-->
    <xsl:param name="from_source"/>


    <xsl:template match="/">
        <xsl:element name="html">
            <xsl:element name="head">
                <xsl:element name="title">Задание 4</xsl:element>
            </xsl:element>
            <xsl:element name="body">
                <xsl:element name="ul">
                    <xsl:attribute name="style">list-style-type: none;</xsl:attribute>
                    <xsl:choose>
                        <!--для задания (б)-->
                        <xsl:when test="normalize-space($from_source)='true'">
                            <xsl:variable name="source_outside" select="exsl:node-set(document('menu_source.xml'))/root"/>
                            <xsl:call-template name="draw_menu">
                                <xsl:with-param name="content">
                                    <xsl:apply-templates select="$source_outside/project/page" mode="item"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <!--для задания (a)-->
                            <xsl:apply-templates select="exsl:node-set($menu)/item"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>


<!--вывод меню по шаблону-->
    <xsl:template match="item">
        <xsl:element name="li">
            <xsl:choose>
                <xsl:when test="url[@is_current='true()']">
                    <xsl:element name="span">
                        <xsl:value-of select="self::node()/title"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:value-of select="self::node()/url"/>
                        </xsl:attribute>
                        <xsl:value-of select="self::node()/title"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>


    <!--для задания(б)  составление  правильного nodeset,чтобы match="item" сработал-->
    <xsl:template match="root/project/page" mode="item">
        <xsl:param name="urlparam">
            <xsl:copy-of  select="parent::node()/preceding::node()/url"/>
        </xsl:param>
        <xsl:param name="hostparam">
            <xsl:copy-of  select="parent::node()/preceding::node()/host"/>
        </xsl:param>
        <xsl:element name="item">
            <xsl:element name="title">
                <xsl:value-of select="@name"/>
            </xsl:element>
            <xsl:element name="url">
                <xsl:attribute name="is_current">
                    <xsl:choose>
                        <xsl:when test="current()=$urlparam">true()</xsl:when>
                        <xsl:otherwise>false()</xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="current()='/'"> <xsl:value-of select="$hostparam"/></xsl:when>
                    <xsl:otherwise> <xsl:value-of select="concat($hostparam,current())"/></xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:element>
    </xsl:template>


<!--для задания(б) передача правильного nodeset в match="item"-->
    <xsl:template name="draw_menu">
        <xsl:param name="content"/>
        <xsl:apply-templates select="exsl:node-set($content)/item"/>
    </xsl:template>



</xsl:stylesheet>
