<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:strip-space elements="*"/>

    <!--ВЦКП-->
    <xsl:variable name="utilities" select="'shop.xml?scid=3681'" />
    <!--петроэлектросбыт-->
    <xsl:variable name="electricity" select="'shop.xml?scid=5670'" />
    <!--газ-->
    <xsl:variable name="gas" select="'shop.xml?scid=3746'" />
    <!--твой интернет-->
    <xsl:variable name="tvoiinternet" select="'shop.xml?scid=958'" />
    <!--триколор-->
    <xsl:variable name="colortv" select="'shop.xml?scid=4218'" />
    <!--ростелеком-->
    <xsl:variable name="rostelecom" select="'shop.xml?scid=3535'" />
    <!--штрафы-->
    <xsl:variable name="fines" select="'debts'" />
    <!--моб.телеф-->
    <xsl:variable name="mobile" select="'shops-phone.xml?ctid=157291'" />


    <xsl:template match="/">
        <html>
            <body>
                <xsl:apply-templates select="/yandex_payments/payments[@type='gorod payments pop' and @url='gorod']/payment"/>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="payment">
        <!--тут можно выводить формы как угодно,если надо одну на странице, то через choose,а можно и один if оставить
        но щас наши дизайнеры  попросили вывести две формы на странице-с оплатой телефона и штрафы ГИБДД,причем захотели чтоб была возможность
        несколько инф.сообщений к форме добавить-->
        <xsl:if test="boolean(./@url = $rostelecom)">
            <form method="POST" action="/{@url}">
                <input type="text" value="{@name}"/><span>Название</span><br/>
                <input type="text"/><span>Номер телефона</span><br/>
                <input type="text"/><span>Сумма</span><br/>
                <input type="submit" value="заплатить"/>
            </form>
                <xsl:apply-templates select="document('temporary_changes.xml')/temporary_changes/change[@code='bonus' or @code='delay']"/>
        </xsl:if>


        <xsl:if test="boolean(./@url = $fines)">
            <form method="POST" action="/{@url}">
                <input type="text"/><span>Водительское удостоверение</span><br/>
                <input type="text"/><span>Свидетельство о регистрации ТС</span><br/>
                <input type="submit" value="Проверить"/>
            </form>
            <xsl:apply-templates select="document('temporary_changes.xml')/temporary_changes/change[@code='delay']"/>
        </xsl:if>


    </xsl:template>

    <xsl:template match="change">
        <div class="message-info">
            <h2 style="color:{@color}"><xsl:value-of select="title"/></h2>
            <p><xsl:value-of select="section"/></p>
        </div>
    </xsl:template>

</xsl:stylesheet>

